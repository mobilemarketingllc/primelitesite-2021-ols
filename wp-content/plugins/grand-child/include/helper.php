<?php


// Area Rug Gallery Images
function arearugGallery(){
    $images = array('https://s3-us-west-2.amazonaws.com/rugs.shop/karastan-rug39600_21004_096132_rm01.jpeg',
    'https://s3-us-west-2.amazonaws.com/rugs.shop/karastan_spicemarket_room.jpg',
    'https://uca50882c99ba8a2860406828188.previews.dropboxusercontent.com/p/thumb/AAYrZgEqNjW10QlsL6znTRSpWOTvrXMa8gch4K7a6cSP7GxcC2eTEJVtIF1YLQqQ4MtA17ZDECRdsIqFHPiVbJPMBvtxxMrag_0BFVQdXjJBeF5kX4OabIafJfKInSQ8aCLUePfpex5YgzR8FaoADB16SDDWXdq-m056RHCYlr3pjJwaM_8ydNpvUcLxff1iRxxJBy6Me8Z6OQD8wndk-VlQa-481zCSy6OxNl1FIu3Uqx5nrGsNsWYbcmbZxwWZD8ohzAky4nq7GzKg-dsZfaGLy3lJG4AjAqksCmEAXItc6OPGDy0jcmO7oH8IvziD5MwSZxD07I93IrrahuIb9PJyZ7HyzuRsWhZSo6mCB_B6PXJyU5ryoSu-Bj6t4uk7wkQ/p.jpeg?fv_content=true&size_mode=5',
    'https://uc2e3078cf6c3e0d545bd957cf31.previews.dropboxusercontent.com/p/thumb/AAZE1uy_l2MCT3cYoFn745snlAv2OVTcOQInTt3_Sa064IOp-t24t0wguXQrWg11RY0HxHKihO5Zl9gN98LiLOJqLwR6LQHWYmkPwYSDZWlBRZGQasdkPwBOAB_pIvo-XeyzeaA1KMm3KqMTT85Ma9sMJ9qVNYIvNXy7AOMDUHJePCRa_KhH23jDbVWPxi2ZvFAdhZhitzPZPn8WPdVuixr_dOCe4jE6iLdhMqJk6TFMD0kORizZMK1C9gO4N3TZS7kXZAhKmajOPf2D2kpmG-YzRz2cNXZslRP53-0GQ-j8_YvQlojvSwPf9M5VME95731mD9nnCVKgY9UngYwm7_LCa7HcHsqyDwlDLa1fI_TS-WnAby_24RKH4oWZ7_cXv5o/p.jpeg?fv_content=true&size_mode=5',
    'https://ucba04b34212dce332a9933719e8.previews.dropboxusercontent.com/p/thumb/AAas2RqP6kRooymq7cVEh0uQ1FZKLiGQZAeAEaEZfo3G53bWMj27cQq9-6gFuCTOdN-aTmlPkbH0VIqb7dNT6TeSChXlUn7R2wpMAP7_Qf_wcYG5DRupL19-HWXOlXgtsJGUiND5cZgNqyNFpEsgJvwyn8Gr9sD96G5xwDpfvivGZIm3xfhhLXpVqDB5KPCBSjDgMLkweyt5dAnRdXOINKewYIrM_sQ4pC3pnuDvMWjvKbozCNozeC1l8HzDCun0Rmsf39lhsbZxDtm363OumjIHg7TNXqrU_UfqTASbt0oFU2IzBWYqZ_OMThjoZrDy6eB5E30VE-4wLYw8KWDmqHNKU9mACUK32EMNer3p3KJ50Gc3CWoqTo2CLbpF2vG1MxY/p.jpeg?fv_content=true&size_mode=5',
    'https://ucdc27270fea2babaaac19b88061.previews.dropboxusercontent.com/p/thumb/AAbOn4adlb_8IwURv-llTeymNwOvJWYNAaP0eDKdTFtDwTIoNLCEdA-XRPeKHpJrubOX4c3Z0t_s1nI2opzeKWsrjevVbHisdMOs7pqF0ntBa8bNE0Nc0hDLy0R0BeFXFGphX3VXXezjYZ_c7je8q_60KRiq9a2noTZKaaS_FaP1lAYyq2ohtlH88vjY-sdBAjH2HAfieAnjwRlsVJjtGrB8WUCzj2GOjCKg_9lU_usUQ3BnlbURVJRqQRfDuJCT7OtMWrRpCCjWJkZ1dw59PMpiAdZegMrVkv6-Ij7-X_l7z0o1aEj7Ijbsdzr6dFH7yGTaJwOH0NllL5Id3cGsFN3iLn6TE4rZfFNVOd08DQCAT1JK-5DlVkLl8fhbwlWttIU/p.jpeg?fv_content=true&size_mode=5',
    'https://ucaec0b4b093dcbae9802224b95b.previews.dropboxusercontent.com/p/thumb/AAasCTLUctRx-y2UclCA76Q3V67KjRic917MI19JJ9JSAftfDMh64golgYPJjyaxjpf3TUZ5kO_OikcOH3Lszk0WpfRLbIZcxOPVU05bEKy55ATYlwtHy6wAKuIBs-ho7fSb135-HVL1eRA5TZ8Nu2z92djfBYSbK503EjxfCeIUmwRpKJIa-jtM9941hE3giCEcnCkeWKMK92PWx5t1CvbHbaxUAlCLoaIuZB3EeDZFQ7T3p64OSdqXFUj_W26gnP5Yv34zMe7CIc3axsyVEiWZJB-3VFEwfDmXklQLR2bwtxwxe4dxTqYIXlfArWSHhkACl0IMWeqi6eM-XwYVEE3PCw6jmF5D5tzAQxUnh5_QBH-owh3aLZNr_rGjkt_lOHM/p.jpeg?fv_content=true&size_mode=5',
    'https://uc67211dd60046004e95f15e0495.previews.dropboxusercontent.com/p/thumb/AAZdC9zFI5NgLZAdAChU-wCguOAeO6yNa2lKcljDwWwsgBGdRstrFr7PQuaUUssEm0VFEB05XO5wptwcwEbURIfZIWOE417C5PrXQw6xrKPTIAQ0Br0WkDQItxb4s_u3K-DLWvKWLES_HT4rctxKZMeGBLCu2BPgNBIlxnFG5yTYuZrqoahw9yb3aDbd3Cjk2Cr8nF2YfDc-wvoRkb8cgGkqkvirVeRECWpr5iu16cynEbQ_nm6mOpBxIn42Ox74ZGnRWTvzxJgqA9ogbyO5HbOoxneYrY-wD-CzreQdYTEWMc-qD4R-QUJVyvC418Z2ypXkQomhxhQ7rK9Yqd_DJOpE8fdbi1CH2dWRHaGYZXE1ftGTYYzus2IjzYsyP6akwEM/p.jpeg?fv_content=true&size_mode=5',
    'https://uca876d78c774cebc7e8a922bd2f.previews.dropboxusercontent.com/p/thumb/AAYBSM1QFcYvAoEYKIZfwg4ELQhz9k6bq81Ucdixcym-zeAAiQ3__Kl0rAQAtHzzClrD9UDn-UGh53bf_9hy8EVWV6uq-z1rDQL58UKng4i_bio-X0TqRdPQMhqDGpDkOSFqwhYm2KIIkGsKM_PbSkF2uf40Vn-PzrKjIxVQWgkNPoNQaFW9WL2hiPHo4If9Rm2G9Km75mCs0ew1liW1t1fiN8_Pe54l-VjkMOpPkntKXI3aQJsc2ho0d3L7aa2cAL4ADBRlUtweiYgyqVWlh8NW-UC524SqKBFDUpWAnxfwc5ZvYQNeOCx27BwXhxX-9gfqYC0suI0tennLIplK6P8d3qDSIPluQHfShtmXM0YiZvMXXyY4TojpbW3S2cx6SZM/p.jpeg?fv_content=true&size_mode=5',
    'https://ucc2edb556adee3cdcab67e2fd3a.previews.dropboxusercontent.com/p/thumb/AAbIJ8tW2XcmxqiSJZ9BVk8n7s9cLf7W6PXi0xW8uLnzWk0rx_HShpmvs9r7es4ec2irQocLsxP1-MRVngLJEPTv1F-Ng9PQ9fzWKCs-OLKvTmqiXBqpC0F-1tyg6-hmIDtfvS1dfGQ4Em5Prl6c2rulAuRVW7wbshgd79sMAsireyYYJnSFpqVnwKXgNwluUSp9wj0jUsT3r-JeueYunJNNvChC0zq6RmR2R4cIrYV6VhZq5QqjAG04S1GM_tVapvTLH_VhCkFhIaDrwJPu0gt4-uXcM2Ooo-wGoeYRqhFRH7WkrO7rbiH6rAAgzXLfIJdiNLQHffpSwWYxZ9wgKA0XTONFR2M2JhQ44nAae0xDCU4w66ML_5CRL2kMrmrtvBc/p.jpeg?fv_content=true&size_mode=5',
    'https://uc3bb3105e96b307b6e8af595264.previews.dropboxusercontent.com/p/thumb/AAYryib-9JHRSzDKOA542-KtBQwyzxF47AKx0-9YXfg7VRd21rCDhtF3aoevV6QQguBtVO4Ld_LA73R4nXD38w1bAvPxMzMCZKjLJOmlISFjLrqzB8EOLyLrDS0jp9QNS4QZeompNQlg2-CM3qZn3KfGH_fpi_Td5i8Lb8IPdazAfctxhyP-yxX5uXjwMCFNE-NnIWEw1TD_SHW1LceKZdQ4H-wRX4zckpBcFN4bNs7Ic3k9pk_lJUhq7x5hxvgfutUrWns1qrnDArRvrWigphTjU0bnBO7wZ1GLTfCoBq60BnLjCcKGEWo1M7CEAedULERnkd3Av8-BxA-ji6OtQ34_yiYwG58tV_aLKmS24Aghc-H3foGVf744JvoXgSJnOUI/p.jpeg?fv_content=true&size_mode=5');

$result = '<div id="lightgallery" class="rug-gallery">';
 foreach ($images as $img) {
     $result .= '<a href="'.$img.'"><img src="'.$img.'" /></a>';
 }
    $result .= '</div>';
    return $result;
}
add_shortcode('areagallery', 'arearugGallery');
//Tranding Product ShortCode

function shopofarearug($arg)
{
    $website_json =  json_decode(get_option('website_json'));
    $rugAffiliateCode = isset($website_json->rugAffiliateCode)?$website_json->rugAffiliateCode:"";
    $data = array(
        array(
            'url'=>'https://rugs.shop/area-rugs/rug-colors/?store='.$rugAffiliateCode,
            'image_url'=> plugins_url('../img/Color.png', __FILE__),
            'title'=> "Shop by Color"
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/rug-styles/?store='.$rugAffiliateCode,
            'image_url'=> plugins_url('../img/style.png', __FILE__),
            'title'=> "Shop by Style"
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/rug-sizes/?store='.$rugAffiliateCode,
            'image_url'=> plugins_url('../img/Size.png', __FILE__),
            'title'=> "Shop by Size"
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/rug-patterns/?store='.$rugAffiliateCode,
            'image_url'=> plugins_url('../img/patterns.png', __FILE__),
            'title'=> "Shop by Patterns"
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/rug-brands/?store='.$rugAffiliateCode,
            'image_url'=> plugins_url('../img/brands.png', __FILE__),
            'title'=> "Shop by Brands"
        ),
        array(
            'url'=>'https://rugs.shop/on-sale/?store='.$rugAffiliateCode,
            'image_url'=> plugins_url('../img/sale.png', __FILE__),
            'title'=> "Area Rug Sale"
        ),
       
    );
    $result ="";
    $result.= '<div class="products-list column-3"><ul class="product-list">';
    for ($i=0;$i<count($data);$i++) {
        $result .='<li class="product-li-three-row">
                    <div class="product-inner">
                        <div class="product-img-holder">
                            <a href="'.$data[$i]['url'].'" target="_blank" itemprop="url" class="">
                                <img src="'.$data[$i]['image_url'].'" alt="'.$data[$i]['title'].'" />
                            </a>
                        </div>
                        <h3 class="fl-callout-title"><a href="'.$data[$i]['url'].'" target="_blank">'.$data[$i]['title'].'</a></h3>
                    </div>
                </li>';
    }
    $result.= '</ul></div>';
    return $result;
}

add_shortcode('shoparearug', 'shopofarearug');

//Tranding Product ShortCode

function arearugProductList($arg)
{
    $website_json =  json_decode(get_option('website_json'));
    $rugAffiliateCode = isset($website_json->rugAffiliateCode)?$website_json->rugAffiliateCode:"";
    
    $data = array(
        array(
            'url'=>'https://rugs.shop/area-rugs/?store='.$rugAffiliateCode,
            'image_url'=>'https://mm-media-res.cloudinary.com/image/fetch/h_415,w_280,c_limit/http://mmllc-images.s3.amazonaws.com/unitedweavers/3900201016320aqua.jpg',
            'price'=>'99.00',
            'title'=> "PETRA MULTI",
            'collection' => 'Spice Market',
            'brand' => 'Karastan'
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/?store='.$rugAffiliateCode,
            'image_url'=>'https://mm-media-res.cloudinary.com/image/fetch/h_415,w_280,c_limit/http://mmllc-images.s3.amazonaws.com/unitedweavers/3900201016320aqua.jpg',
            'price'=>'499.00',
            'title'=> "Esperance Seaglass",
            'collection' => 'Titanium Collection',
            'brand' => 'Karastan'
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/?store='.$rugAffiliateCode,
            'image_url'=>'https://mm-media-res.cloudinary.com/image/fetch/h_415,w_280,c_limit/http://mmllc-images.s3.amazonaws.com/unitedweavers/3900201016320aqua.jpg',
            'price'=>'156.00',
            'title'=> "Nirvana Indigo",
            'collection' => 'Cosmopolitan',
            'brand' => 'Karastan'
        ),
        array(
            'url'=>'https://rugs.shop/area-rugs/?store='.$rugAffiliateCode,
            'image_url'=>'https://mm-media-res.cloudinary.com/image/fetch/h_415,w_280,c_limit/http://mmllc-images.s3.amazonaws.com/unitedweavers/3900201016320aqua.jpg',
            'price'=>'62.40',
            'title'=> "7139a",
            'collection' => 'Andora',
            'brand' => 'Oriental Weavers'
        )
    );
    $result ="";
    $result.= '<div class="products-list"><ul class="product-list">';
    for ($i=0;$i<count($data);$i++) {
        $result .='<li class="product-li">
            <div class="product-inner">
                <div class="product-img-holder">
                    <a href="'.$data[$i]['url'].'" target="_blank"><img src="'.$data[$i]['image_url'].'" alt="'.$data[$i]['title'].'" /></a>
                </div>
                <div class="product-info">
                    <h6>'.$data[$i]['collection'].'</h6>
                    <h4><a href="'.$data[$i]['url'].'" target="_blank">'.$data[$i]['title'].'</a></h4>

                    <div class="price-section">
                        <span>Starting at</span>
                        <span class="price"> $'.$data[$i]['price'].'</span>
                        <span class="sale">On Sale</span>
                    </div>

                    <div class="button-section">
                        <a href="'.$data[$i]['url'].'" class="button" target="_blank">Buy Now</a>
                        <div class="brand-wrap">By '.$data[$i]['brand'].'</div>
                    </div>
                </div>
            </div>
        </li>';
    }
    $result.= '</ul></div>';
    return $result;
}

add_shortcode('area_rug_trading_products', 'arearugProductList');



function formatPhoneNumber($phoneNumber)
{
    $phoneNumber = preg_replace('/[^0-9]/', '', $phoneNumber);

    if (strlen($phoneNumber) > 10) {
        $countryCode = substr($phoneNumber, 0, strlen($phoneNumber)-10);
        $areaCode = substr($phoneNumber, -10, 3);
        $nextThree = substr($phoneNumber, -7, 3);
        $lastFour = substr($phoneNumber, -4, 4);

        $phoneNumber = '+'.$countryCode.' ('.$areaCode.') '.$nextThree.'-'.$lastFour;
    } elseif (strlen($phoneNumber) == 10) {
        $areaCode = substr($phoneNumber, 0, 3);
        $nextThree = substr($phoneNumber, 3, 3);
        $lastFour = substr($phoneNumber, 6, 4);

        $phoneNumber = '('.$areaCode.') '.$nextThree.'-'.$lastFour;
    } elseif (strlen($phoneNumber) == 7) {
        $nextThree = substr($phoneNumber, 0, 3);
        $lastFour = substr($phoneNumber, 3, 4);

        $phoneNumber = $nextThree.'-'.$lastFour;
    }

    return $phoneNumber;
}

function storelocation_address($arg)
{
    
    $website = json_decode(get_option('website_json'));
    $weekdays = array("monday","tuesday","wednesday","thursday","friday","saturday","sunday");
    $locations = "<ul class='storename'><li>";
    
    
    for ($i=0;$i<count($website->locations);$i++) {
        $location_name = isset($website->locations[$i]->name)?$website->locations[$i]->name:"";

        if($website->locations[$i]->type == 'store' &&  $website->locations[$i]->name !=''){

          //  if (in_array(trim($location_name), $arg)){

                if($website->locations[$i]->name == 'Main' || $website->locations[$i]->name=="MAIN"){

                    $location_name = get_bloginfo( 'name' );
                    
                }else{
                  
                    $location_name =  isset($website->locations[$i]->name)?$website->locations[$i]->name." ":"";

                }


                $location_address_url =  $location_name;
                $location_address_url  .= isset($website->locations[$i]->address)?$website->locations[$i]->address." ":"";
                $location_address_url .= isset($website->locations[$i]->city)?$website->locations[$i]->city." ":"";
                $location_address_url .= isset($website->locations[$i]->state)?$website->locations[$i]->state." ":"";
                $location_address_url .= isset($website->locations[$i]->postalCode)?$website->locations[$i]->postalCode." ":"";
        
                $location_address  = isset($website->locations[$i]->address)?"<p>".$website->locations[$i]->address."</p>":"";
                $location_address .= isset($website->locations[$i]->city)?"<p>".$website->locations[$i]->city.", ":"<p>";
                $location_address .= isset($website->locations[$i]->state)?$website->locations[$i]->state:"";
                $location_address .= isset($website->locations[$i]->postalCode)?" ".$website->locations[$i]->postalCode."</p>":"</p>";
                
                $location_phone  = "";
                if (isset($website->locations[$i]->phone)) {
                    $location_tel = preg_replace('/[^0-9]/', '', $website->locations[$i]->phone);
                    $location_phone  = formatPhoneNumber($website->locations[$i]->phone);
                }
                if (isset($website->locations[$i]->forwardingPhone) && $website->locations[$i]->forwardingPhone != "") {
                    $forwarding_tel = preg_replace('/[^0-9]/', '', $website->locations[$i]->forwardingPhone);
                    $forwarding_phone  = formatPhoneNumber($website->locations[$i]->forwardingPhone);
                    
                }else if (isset($website->locations[$i]->phone) && $website->locations[$i]->forwardingPhone = "") {

                    $location_tel = preg_replace('/[^0-9]/', '', $website->locations[$i]->phone);
                    $location_phone  = formatPhoneNumber($website->locations[$i]->phone);
                }
                else{
        
                    $forwarding_tel ="#";
                    $forwarding_phone  = formatPhoneNumber(8888888888);
                }


                if (in_array("loc", $arg)) {

                    if (in_array('nolink', $arg)){
                        $locations .= '<div class="store-container">';
                        //$locations .= '<div class="name">'.$location_name.'</div>';
                        $locations .= '<div class="address">'.$location_address.'</div>';
                        //$locations .= "<div class='phone'><a href='tel:".$location_tel."'>"."<span>".$location_phone."</span></a></div>";
                        $locations .= '</div>';
                    }else{

                        $locations .= '<div class="store-container">';
                        //$locations .= '<div class="name">'.$location_name.'</div>';
                        $locations .= '<div class="address"><a href="https://maps.google.com/maps/?q='.urlencode($location_address_url).'" target="_blank" '.'>'.$location_address.'</a></div>';
                        //$locations .= "<div class='phone'><a href='tel:".$location_tel."'>"."<span>".$location_phone."</span></a></div>";
                        $locations .= '</div>';

                    }
                        
                    
                }
                if (in_array("forwardingphone", $arg)) {

                        if (in_array('nolink', $arg)){
                            $locations .= "<div class='phone'><span>".$forwarding_phone."</span></div>";
                        }
                        else{
                            $locations .= "<div class='phone'><a href='tel:".$forwarding_tel."'><span>".$forwarding_phone."</span></a></div>";
                        }

                }
                if (in_array("ohrs", $arg)) {
                    $locations .= '<div class="store-opening-hrs-container">';
                    $locations .= '<ul class="store-opening-hrs">';

                    for ($j = 0; $j < count($website->locations[$i]->hours); $j++) {

                    //   write_log( $website->locations[$i]->hours[$j]->day);
                      
                        if(isset($website->locations[$i]->hours[$j]->hours)) {
                           
                            $locations .= '<li>'.$website->locations[$i]->hours[$j]->day.': <span>'.$website->locations[$i]->hours[$j]->hours.'</span></li>';
                          
                       }
                    }
                    $locations .= '</ul>';
                    $locations .= '</div>';
                }
                if (in_array("dir", $arg)) {
                    
                        $locations .= '<div class="direction">';
                        $locations .= '<a href="https://maps.google.com/maps/?q='.urlencode($location_address_url).'" target="_blank" class="fl-button" role="button">
                                            <span class="button-text">GET DIRECTIONS</span></a>';
                        $locations .= '</div>';
                    
                }
                if (in_array("map", $arg)) {
                    
                        $locations .= '<div class="map-container">
                        <iframe src="https://www.google.com/maps/embed/v1/place?key='.GOOGLE_MAP_KEY.'&amp;q='.urlencode($location_address_url).'"&zoom=nn style="width:100%;min-height:450px;border:0px;"></iframe>
                        </div>';
                    
                }
                if (in_array("phone", $arg)) {
                            $locations .= "<div class='phone'><a href='tel:".$location_tel."'>"."<span>".$location_phone."</span></a></div>";
                }
          //  } 

        }        
    }
$locations .= '</li>';
$locations .= '</ul>';
    
    return $locations;
}
    add_shortcode('storelocation_address', 'storelocation_address');


function contact_phonenumber($atts)
{
    if (get_option('api_contact_phone')) {
        return '<a href="tel:'.get_option('api_contact_phone').'" target="_self" class="fl-icon-text-link fl-icon-text-wrap">'.get_option('api_contact_phone').'</a>';
    } else {
        return '<a href="javascript:void(0)" target="_self" class="fl-icon-text-link fl-icon-text-wrap">(XXX) (XXXXXXX)</a>';
    }
}
add_shortcode('contactPhnumber', 'contact_phonenumber');


function getSocailIcons()
{
    $details = json_decode(get_option('social_links'));
    $return  = '';
    if (isset($details)) {
        $return  = '<ul class="social-icons">';
        foreach ($details as $key => $value) {
            if ($value->group =="social") {
              
            if ($value->platform=="linkedIn") {
                $value->platform = "linkedin";
            }
            
                
                
            if ($value->active) {
                $return  .= '<li><a href="'.$value->url.'" target="_blank" title="'.$value->platform.'"><i class="fab fa-'.strtolower($value->platform).'"></i></a></li>';
            }
        }
        }
        $return  .= '</ul>';

    }
    return $return;
}
add_shortcode('getSocailIcons', 'getSocailIcons');


function create_post_type()
{
    register_post_type(
        'store-locations',
        array(
        'labels' => array(
          'name' => __('Store Locations'),
          'singular_name' => __('Store Location')
        ),
        'public' => true,
        'has_archive' => true,
      )
    );
}
add_action('init', 'create_post_type');

function contactInformation($atts)
{
    $contacts = json_decode(get_option('website_json'));
    $info="";
    if (is_array($contacts->contacts)) {
        if (in_array("withicon", $atts)) {
            for ($i=0;$i<count($contacts->contacts);$i++) {
                if (in_array($contacts->contacts[$i]->type, $atts)) {
                    if (in_array("email", $atts)) {
                        $info  .= "<a class='mail-icon icon-link' href='mailto:".$contacts->contacts[$i]->email."'>".$contacts->contacts[$i]->email."</a>";
                    }
                    if (in_array("phone", $atts)) {
                        $info  .= "<a class='phone-icon icon-link' href='tel:".preg_replace('/[^0-9]/', '', $contacts->contacts[$i]->phone)."'>".formatPhoneNumber($contacts->contacts[$i]->phone)."</a>";
                    }
                    if (in_array("notes", $atts)) {
                        $info  .=  $contacts->contacts[$i]->notes;
                    }
                }
            }
        } else {
            for ($i=0;$i<count($contacts->contacts);$i++) {
                if (in_array($contacts->contacts[$i]->type, $atts)) {
                    if (in_array("email", $atts)) {
                        $info  .= "<a class='mail-icon-with icon-link-with' href='mailto:".$contacts->contacts[$i]->email."'>".$contacts->contacts[$i]->email."</a>";
                    }
                    if (in_array("phone", $atts)) {
                        $info  .= "<a class='phone-icon-with icon-link-with' href='tel:".preg_replace('/[^0-9]/', '', $contacts->contacts[$i]->phone)."'>".formatPhoneNumber($contacts->contacts[$i]->phone)."</a>";
                    }
                    if (in_array("notes", $atts)) {
                        $info  .=  $contacts->contacts[$i]->notes;
                    }
                }
            }
        }
    }
    return $info;
}
add_shortcode('contactInformation', 'contactInformation');


function locationInformation($atts)
{
    $website = json_decode(get_option('website_json'));
    
    for ($i=0;$i<count($website->locations);$i++) {

        if($website->locations[$i]->type == 'store'){

        if (in_array($website->locations[$i]->name, $atts)) {
            if (in_array("license", $atts)) {
                $info  .= "<div class='store-license'>".$website->locations[$i]->licenseNumber."</div>";
            }
            if (in_array("phone", $atts)) {
                $info  .= "<a class='phone-icon-with icon-link-with' href='tel:".preg_replace('/[^0-9]/', '', $website->locations[$i]->phone)."'>".formatPhoneNumber($website->locations[$i]->phone)."</a>";
            }
        }
    }
    }
    return $info;
}
add_shortcode('locationInformation', 'locationInformation');


function hookSharpStrings() {

    $website_json =  json_decode(get_option('website_json'));
    $sharpspringDomainId =$sharpspringTrackingId ="";
    for($j=0;$j<count($website_json->sites);$j++){
        
        if($website_json->sites[$j]->instance == ENV){
           
                $sharpspringTrackingId = $website_json->sites[$j]->sharpspringTrackingId;
                $sharpspringDomainId = $website_json->sites[$j]->sharpspringDomainId;
           
            }
        }
        
       if(isset($sharpspringTrackingId) && trim($sharpspringTrackingId) !="" ){
   
    ?>
           <script type="text/javascript">

                    var _ss = _ss || [];

                    _ss.push(['_setDomain', 'https://<?php echo $sharpspringDomainId;?>.marketingautomation.services/net']);

                    _ss.push(['_setAccount', '<?php echo $sharpspringTrackingId;?>']);

                    _ss.push(['_trackPageView']);

                    (function() {

                    var ss = document.createElement('script');

                    ss.type = 'text/javascript'; ss.async = true;

                    ss.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + '<?php echo $sharpspringDomainId;?>.marketingautomation.services/client/ss.js?ver=1.1.1';

                    var scr = document.getElementsByTagName('script')[0];

                    scr.parentNode.insertBefore(ss, scr);

                    })();

                    </script>
    <?php
       }
}
add_action('wp_head', 'hookSharpStrings');

function getRetailerInfo($arg){


    $website = json_decode(get_option('website_json'));

    for ($i=0;$i<count($website->locations);$i++) {
       

        if($website->locations[$i]->type == 'store' &&  $website->locations[$i]->name !=''){


write_log($website->locations[$i]);
                if (in_array('city', $arg)){
                    $info =  isset($website->locations[$i]->city)?"<span class='city retailer'>".$website->locations[$i]->city."</span>":"";
                }
                else if (in_array('state', $arg)){
                    $info =  isset($website->locations[$i]->state)?"<span class='state retailer'>".$website->locations[$i]->state."</span>":"";
                }
                else if (in_array('zipcode', $arg)){
                    $info =  isset($website->locations[$i]->postalCode)?"<span class='zipcode retailer'>".$website->locations[$i]->postalCode."</span>":"";
                }
                else if (in_array('legalname', $arg)){
                    $info =  isset($website->name)?"<span class='name retailer'>".$website->name."</span>":"";
                }
                else if (in_array('address', $arg)){
                    $info =  isset($website->locations[$i]->address)?"<span class='street_address retailer'>".$website->locations[$i]->address."</span>":"";
                }
                else if (in_array('phone', $arg)){
                    if (isset($website->locations[$i]->phone)) {
                        $location_tel = preg_replace('/[^0-9]/', '', $website->locations[$i]->phone);
                        $location_phone  = formatPhoneNumber($website->locations[$i]->phone);
                    }
                    if (in_array('nolink', $arg)){
                        $info =  isset($website->locations[$i]->phone)?"<span class='phone retailer'>".$location_phone."</span>":""; 
                    }
                    else{
                        
                        $info =  isset($website->locations[$i]->phone)?"<a href='tel:".$location_tel."' class='phone retailer'>"."<span>".$location_phone."</span></a>":"";
                    }
                }
                else if (in_array('forwarding_phone', $arg)){
                    if (isset($website->locations[$i]->forwardingPhone)) {
                        $location_tel = preg_replace('/[^0-9]/', '', $website->locations[$i]->forwardingPhone);
                        $location_phone  = formatPhoneNumber($website->locations[$i]->forwardingPhone);
                    }
                    if (in_array('nolink', $arg)){
                        $info =  isset($website->locations[$i]->forwardingPhone)?"<span class='phone retailer'>".$location_phone."</span>":""; 
                    }
                    else{
                        
                        $info =  isset($website->locations[$i]->forwardingPhone)?"<a href='tel:".$location_tel."' class='phone retailer' >"."<span>".$location_phone."</span></a>":"";
                    }
                }
                else if (in_array('companyname', $arg)){
                    for($j=0;$j<count($website->sites);$j++){
                    
                        if($website->sites[$j]->instance == ENV){
                                $info =  isset($website->sites[$j]->name)?"".$website->sites[$j]->name."":"";
                                break;
                            }
                        }
                }
                else if (in_array('site_url', $arg)){
                    for($j=0;$j<count($website->sites);$j++){
                    
                        if($website->sites[$j]->instance == ENV){
                                $info =  isset($website->sites[$j]->url)?"<span class='site_url retailer'>".$website->sites[$j]->url."</span>":"";
                                break;
                            }
                        }
                }
            }

    }
    
    return $info;  
}

add_shortcode('Retailer', 'getRetailerInfo');


function updateSaleCoupanInformation($arg){
    $promos = json_decode(get_option('promos_json'));

    $saleinformation = json_decode(get_option('saleinformation'));
    $saleinformation = (array)$saleinformation;

    $salespriority = json_decode(get_option('salespriority'));
    $salespriority = (array)$salespriority;
    $high_priprity = min($salespriority);


    if(isset($_GET['promo']) && !empty($_GET['promo'])) {

        $promo_code = $_GET['promo'];

    }else{ 

        $promo_code = array_search($high_priprity,$salespriority);
    }

    
   $div='';

   $website_json =  json_decode(get_option('website_json'));
   $rugAffiliateCode = isset($website_json->rugAffiliateCode)?$website_json->rugAffiliateCode:"";
   $loc_country = $website_json->locations[0]->country;
    if($loc_country =='US'){ $en = 'en_us'; }elseif($loc_country =='CA'){ $en = 'en_ca'; }
    
    
    $seleinformation = json_decode(get_option('saleinformation'));
    //update_option('saleinformation',json_encode($seleinformation));
    $seleinformation = (object)$seleinformation;

    $salebannerdata = json_decode(get_option('salebannerdata'));
    
        if(in_array('rugshop',$arg) && in_array('salebanner',$arg)){


            foreach($salebannerdata as $bannerinfo){
                
           
                if( $bannerinfo->rugShop == 'true'){  

                    $banner_img_deskop = $bannerinfo->images->desktop;
                    $banner_img_mobile = $bannerinfo->images->mobile;

                    if($bannerinfo->link == ''){

                        $banner_link = 'https://rugs.shop/'.$en.'/?store='.$rugAffiliateCode;

                    }else{

                        $banner_link = 'https://rugs.shop/'.$en.'/'.$bannerinfo->link.'?store='.$rugAffiliateCode;
                    }
                    
                }
            }

            $div = "<div class='salebanner banner-deskop fullwidth rugshopbanner'><a target='_blank' href='".$banner_link."'><img srcset='https://mm-media-res.cloudinary.com/image/fetch/h_200,w_2880,c_limit/".$banner_img_deskop." 4025w, 
            https://mm-media-res.cloudinary.com/image/fetch/h_200,w_2880,c_limit/".$banner_img_deskop." 3019w, 
            https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/".$banner_img_deskop." 2013w, 
            https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/".$banner_img_deskop." 1006w' 
            
            src='https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/".$banner_img_deskop."'  style='width:100%;text-align:center' alt='sales' />
            </a></div>";       
            $div .= "<div class='salebanner banner-mobile rugshopbanner-mobile'><a target='_blank' href='".$banner_link."'><img src='".$banner_img_mobile."' alt='sales' /></a></div>";
         
        }else{        
                  
      
       if(in_array('full',$arg) || in_array('salebanner',$arg)  ){  
         
        if($salebannerdata != ''){
       
            foreach($salebannerdata as $bannerinfo){
                
           
                if( $bannerinfo->priority == $high_priprity ){  

                    $banner_img_deskop = $bannerinfo->images->desktop;
                    $banner_img_mobile = $bannerinfo->images->mobile;
                    $banner_link = $bannerinfo->link;
                }
            }

                if($banner_img_deskop !='' && $banner_img_mobile !=''){

                    $div = "<div class='salebanner banner-deskop fullwidth'><a href='".$banner_link."'><img srcset='https://mm-media-res.cloudinary.com/image/fetch/h_200,w_2880,c_limit/".$banner_img_deskop." 4025w, 
                    https://mm-media-res.cloudinary.com/image/fetch/h_200,w_2880,c_limit/".$banner_img_deskop." 3019w, 
                    https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/".$banner_img_deskop." 2013w, 
                    https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/".$banner_img_deskop." 1006w' 
                    
                    src='https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/".$banner_img_deskop."'  style='width:100%;text-align:center' alt='sales' />
                    </a></div>";

            
                    $div .= "<div class='salebanner banner-mobile'><a href='".$banner_link."'><img src='".$banner_img_mobile."' alt='sales' /></a></div>";

                }


        }
          }
          if(in_array('fixed',$arg) || in_array('salebanner',$arg)){     
            if($salebannerdata != ''){  
           
            foreach($salebannerdata as $bannerinfo){

                if($bannerinfo->priority == $high_priprity){

                    $banner_img_deskop =  $bannerinfo->images->desktop;
                    $banner_img_mobile = $bannerinfo->images->mobile;
                    $banner_link = $bannerinfo->link;
                }
            }

                    if($banner_img_deskop !='' && $banner_img_mobile !=''){
                        
                        $div = "<div class='salebanner banner-deskop fixed'><a href='".$banner_link."'><img srcset='https://mm-media-res.cloudinary.com/image/fetch/h_200,w_2880,c_limit/".$banner_img_deskop." 4025w, 
                        https://mm-media-res.cloudinary.com/image/fetch/h_200,w_2880,c_limit/".$banner_img_deskop." 3019w, 
                        https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/".$banner_img_deskop." 2013w, 
                        https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/".$banner_img_deskop." 1006w' 
                        
                        src='https://mm-media-res.cloudinary.com/image/fetch/h_110,w_1583,c_limit/".$banner_img_deskop."'  style='width:100%;text-align:center' alt='sales' />
                        </a></div>";
    
                
                        $div .= "<div class='salebanner banner-mobile'><a href='".$banner_link."'><img src='".$banner_img_mobile."' alt='sales' /></a></div>";

                    }
          }
          }
        

        }
    if(in_array('categorybanner',$arg)){
     
        if(isset($seleinformation->category_banner_img_deskop)&& $seleinformation->category_banner_img_deskop != ""){ 
        $div = "<div class='salebanner ts banner-deskop'><a href='".$seleinformation->banner_link."'><img src='".$seleinformation->category_banner_img_deskop."' alt='sales' /></a></div>";        
        }
        if(isset($seleinformation->category_banner_img_mobile)&& $seleinformation->category_banner_img_mobile != "")
        $div .= "<div class='salebanner banner-mobile'><a href='".$seleinformation->banner_link."'><img src='".$seleinformation->category_banner_img_mobile."' alt='sales' /></a></div>";
    }
    if(in_array('heading',$arg)){
        if(isset($seleinformation->$promo_code->saveupto_doller)){
            $price = isset($seleinformation->$promo_code->saveupto_doller)?$seleinformation->$promo_code->saveupto_doller:"";
            if (@$_COOKIE['keyword'] ==""  && @$_COOKIE['brand'] == "") {
                
                $div ='<h1 class="googlekeyword">'.$price.'</h1>';
            } else {
                $keyword = $_COOKIE['keyword'];
                $brand = $_COOKIE['brand'];
                $div = '<h1 class="googlekeyword">'.$price.' on '.$brand.' '.$keyword.'<h1>';
            }
        }else{
            $div ='<h1 class="googlekeyword">Check back soon for new promotions</h1>';
        }        
    }

    if(in_array('form_subheading',$arg)){
        // write_log($seleinformation);
        // write_log($salebannerdata);
        if(isset($seleinformation->$promo_code->formTitle)){
            $div = $seleinformation->$promo_code->formTitle;
        }else{
            $div = "Fill out the form below and we'll get back to you shortly.";
        }
    }

     if(in_array('content',$arg)){
        if($seleinformation->$promo_code->formType !="none" ){
         $string = isset($seleinformation->$promo_code->content) ? $seleinformation->$promo_code->content:"";
         $div = '<div class="form-content">'.$string.'</div>';
        }else{
            $div = '<div class="form-content">Although we aren’t running a specific promotion right now, there are still ways to save! Stop by our showroom and meet with one of our flooring experts to learn more about our exceptional products. Our professional team can help you find the ideal flooring for any room in your home.</div>';
        }
    }
    if(in_array('image_onform',$arg)){
        
        if((in_array('backgrondimage',$arg) || in_array('background_img',$arg)) && isset($seleinformation->$promo_code->image_onform)){
                $div = "<div class='salebanner floor-coupon-img  banner-deskop'  id='backgrondimage' ><img src='".$seleinformation->$promo_code->image_onform."' alt='sales' /></div>";
                $div .= "<div class='salebanner floor-coupon-img  banner-mobile' ><img src='".$seleinformation->$promo_code->image_onform_mobile."' alt='sales' /></div>";
        }
        else{

            $div = "<div class='salebanner floor-coupon-img  banner-deskop' id='nosalebgelem'><img src='https://mmllc-images.s3.us-east-2.amazonaws.com/beautifall-financing/nopromo-desktop.jpg' alt='sales' /></div>";
            $div .= "<div class='salebanner floor-coupon-img  banner-mobile'><img src='https://mmllc-images.s3.us-east-2.amazonaws.com/beautifall-financing/no-promo-mobile.jpg' alt='sales' /></div>";
        }
    }

    if(in_array('message',$arg) && isset($seleinformation->$promo_code->message)){
        
        $div = "<div class='message'>".$seleinformation->$promo_code->message."</div>";
       
    }
    if(in_array('navigation',$arg) && isset($seleinformation->navigation_img)){
        if (in_array('image', $arg)) {
            $div = "<div class='navigation_img'><a href='".$seleinformation->navigation_link."'><img src='".$seleinformation->navigation_img."' alt='sales' /></a></div>";
        }
        else if(in_array('text',$arg)){
            $div = "<div class='navigation_img'><a href='".$seleinformation->navigation_link."'>'".$seleinformation->navigation_text."</a></div>";
        }
       
    }
    if (in_array('homepage_banner', $arg)) {
        if (isset($seleinformation->slider) && count($seleinformation->slider) > 0) {
            foreach ($seleinformation->slider as $slide) {
                if (in_array('rugshop', $arg)) {

                    $div = "<div class='rugshop floor-coupon-img' id='homepage-banner-bg' data-bg='".$seleinformation->background_image."'><a href='https://rugs.shop/$en/?store=".$rugAffiliateCode."' target='_blank'><img class='salebanner-slide' src='".$slide->slider_coupan_img."' alt='sales' /></a></div>";
                    break;
                }
                else{
    
                    $div = "<div class='coupon floor-coupon-img' id='homepage-banner-bg' data-bg='".$seleinformation->background_image."'><a href='/flooring-coupon/'><img class='salebanner-slide' src='".$slide->slider_coupan_img."' alt='sales' /></a></div>";
                    break;
                }
            }
        }
    }
	if (in_array('banner', $arg)) {
        if (isset($seleinformation->$promo_code->slider) && count($seleinformation->$promo_code->slider) > 0) {
            foreach ($seleinformation->$promo_code->slider as $slide) {
                $div = "<div class='banner-image' style=''><a href='".$slide->link."'><img class='salebanner-slide' src='".$slide->slider_coupan_img."' alt='sales' /></a></div>";
				break;
            }
        }
    }
    
    
    if (in_array('print_coupon', $arg)) {
        $salebannerdata = json_decode(get_option('salebannerdata'));
        if(is_array($promos) && count($promos) > 0){
            foreach($promos as $promo){
                if($promo->formName == "Shaw Coupon"){
                    if(is_array($promo->widgets) && count($promo->widgets) > 0){
                        foreach($promo->widgets as $widget){
                            if(is_array($widget->images) && count($widget->images) > 0){
                                foreach($widget->images as $image){
                                    if($image->type == "print" ){
                                        
                                        if(strpos($image->url, "http") == false){
                                            $img = "https://".$image->url;
                                        }else{
                                            $img =$image->url;
                                        }
                                        $div = "<a href='".$img."' target='_blank' class='print_coupon_btn btn fl-button'>PRINT COUPON</a>";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }       
    }

    if (in_array('popup_img', $arg)) {
        $salebannerdata = json_decode(get_option('salebannerdata'));
        if(is_array($promos) && count($promos) > 0){
            foreach($promos as $promo){
                if($promo->formName == "Shaw Coupon"){
                    if(is_array($promo->widgets) && count($promo->widgets) > 0){
                        foreach($promo->widgets as $widget){
                            if($widget->type="popup" && is_array($widget->images) && count($widget->images) > 0){
                                foreach($widget->images as $image){
                                    if($image->type == "graphic" ){
                                        if(strpos($image->url, "http") == false){
                                            $img = "https://".$image->url;
                                        }else{
                                            $img =$image->url;
                                        }
                                        $div = "<a href='/flooring-coupon/' class='popup_img_link'><img class='popup_img' src='".$img."' alt='Shaw Popup Image' width='850' height='315'></a>";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }       
    }
    
    return $div;
}

add_shortcode('coupon', 'updateSaleCoupanInformation');

function copyrightstext($arg)
{
    $result = "<p>Copyright ©".do_shortcode('[fl_year]')." ".get_bloginfo().". All Rights Reserved.</p>";
    return $result;
}

add_shortcode('copyrights', 'copyrightstext');


//Colorwall Home Page Banner Shortcode

function colorwall_home_banner($arg)
{
    $product_json =  json_decode(get_option('product_json')); 
 
    foreach ($product_json as $data) {
        foreach ($data as $key => $value) {
           if( $key=="manufacturer" &&   $value== "coretec")
               $isCoretec=true;
        }     
    }
    $coretec_colorwall =  get_option('coretec_colorwall');
    if($coretec_colorwall == '1' && $isCoretec){

       $content = do_shortcode('[fl_builder_insert_layout slug="colorwall-mobile"]');
       $content .=  do_shortcode('[fl_builder_insert_layout slug="colorwall-desktop-banner"]');

       return $content;

    }
}

add_shortcode('colorwallhomebanner', 'colorwall_home_banner');


//Yoast SEO Breadcrumb addded
function bbtheme_yoast_breadcrumbs() {
    if ( function_exists('yoast_breadcrumb') && ! is_front_page() ) {
        yoast_breadcrumb('<div id="breadcrumbs"><div class="container">','</div></div>');
    }
}
add_action( 'fl_content_open', 'bbtheme_yoast_breadcrumbs' );

//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail' );

function wpse_100012_override_yoast_breadcrumb_trail( $links ) {
    global $post;

    if ( is_singular( 'hardwood_catalog' )  ) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/hardwood/',
            'text' => 'Hardwood',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/hardwood/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );

    }elseif (is_singular( 'carpeting' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/carpet/',
            'text' => 'Carpet',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/carpet/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );

    }elseif (is_singular( 'luxury_vinyl_tile' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/vinyl/',
            'text' => 'Vinyl',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/vinyl/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
        
    }elseif (is_singular( 'laminate_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/laminate/',
            'text' => 'Laminate',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/laminate/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
        
    }elseif (is_singular( 'tile_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/tile/',
            'text' => 'Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/tile/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
        
    }elseif (is_singular( 'solid_wpc_waterproof' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/waterproof-flooring/',
            'text' => 'Waterproof Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/waterproof-flooring/products/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
    }

    return $links;
}


  //This filter is executed before displaying each field and can be used to dynamically populate fields with a default value.
  
  add_filter( 'gform_field_value_salepromocode', 'populate_salepromocode' );
  function populate_salepromocode( $value ) {    

    $saleinformation = json_decode(get_option('saleinformation'));
    $saleinformation = (array)$saleinformation;

    $salespriority = json_decode(get_option('salespriority'));
    $salespriority = (array)$salespriority;
    $high_priprity = min($salespriority);

    if (isset($_GET['promo']) && $_GET['promo']!='') {

     return $_GET['promo'];
     
    }elseif(is_array($saleinformation) && count($saleinformation) > 0 ){
        
        foreach($saleinformation as $promo){      
    
            if($promo->priority == $high_priprity && $promo->formType !="none"){
               
                  return $promo->promoCode;
            }else if($promo->formType =="none"){
                return "nopromo";
            }
      
          }
    }else{
        return "nopromo";
    }
  } 

  add_filter( 'gform_field_value_saleformtype', 'populate_saleformtype' );
  function populate_saleformtype( $value ) {
    $saleinformation = json_decode(get_option('saleinformation'));
    $saleinformation = (array)$saleinformation;

    $salespriority = json_decode(get_option('salespriority'));
    $salespriority = (array)$salespriority;
    $high_priprity = min($salespriority);
    if(is_array($saleinformation) && count($saleinformation) > 0 ){
        foreach( $saleinformation as $sale){
        
            if (isset($_GET['promo'])) {
                if( $sale->promoCode == $_GET['promo']){          

                    return $sale->formType;
                    
                }
            }else{

                if($sale->priority == $high_priprity){
                
                    return $sale->formType;
                }
            }
        }
    }else{
        return "defaultfrm";
    }
     
  } 

// Add Yoast SEO sitemap to virtual robots.txt file
function surbma_yoast_seo_sitemap_to_robotstxt_function( $output ) {

    $homeURL = get_home_url();
  $output = '';
      $output .= 'User-agent: *' . PHP_EOL;
      if(get_option('blog_public')=='0'){
      $output .= 'Disallow: /' . PHP_EOL;
      }else{
          $output .= 'Disallow: /wp-admin/' . PHP_EOL;
          $output .= "Allow: /wp-admin/admin-ajax.php". PHP_EOL;
          $output .= "Sitemap: $homeURL/sitemap_index.xml".PHP_EOL;
      }
      

return $output;
}
add_filter( 'robots_txt', 'surbma_yoast_seo_sitemap_to_robotstxt_function', 10, 1 );

//Floorte Home Page Banner Shortcode

function floorte_home_banner($arg)
{   
       $content = do_shortcode('[fl_builder_insert_layout slug="floorte-mobile"]');
       $content .=  do_shortcode('[fl_builder_insert_layout slug="floorte-desktop"]');

       return $content;
}

add_shortcode('floortehomebanner', 'floorte_home_banner');


function featured_products_slider($atts) {

    // print_r($atts);
 
     $args = array(
         'post_type' => $atts['0'],        
         'posts_per_page' => 8,
         'orderby' => 'rand',
         'meta_query' => array(
             array(
                 'key' => 'collection', 
                 'value' => $atts['1'], 
                 'compare' => '=='
             )
         ) 
     );
     $query = new WP_Query( $args );
     if( $query->have_posts() ){
         $outpout = '<div class="featured-products"><div class="featured-product-list">';
         while ($query->have_posts()) : $query->the_post(); 
             $gallery_images = get_field('gallery_room_images');
             $gallery_img = explode("|",$gallery_images);
            
             // loop through the rows of data

             $room_image =  single_gallery_image_product(get_the_ID(),'600','400');
            
 
             $color_name = get_field('color');
             $brand_name = get_field('brand');
             $collection = get_field('collection');
             $post_type = get_post_type_object(get_post_type( get_the_ID() ));
            
             
             $outpout .= '<div class="featured-product-item">
                 <div class="featured-inner">
                     <div class="prod-img-wrap">
                         <img src="'.$room_image.'" alt="'.get_the_title().'" />
                         <div class="button-wrapper">
                             <div class="button-wrapper-inner">
                                 <a href="'.get_the_permalink().'" class="button alt viewproduct-btn">View Product</a>';
              
            if( get_option('getcouponbtn') == 1){

                $outpout .= '<a href="/flooring-coupon/" class="button fea-coupon_btn">Get Coupon</a>';
            }
              
              $outpout .= '</div>
                         </div>
                     </div>
                     <div class="product-info">
                         <div class="slider-product-title"><a href="'.get_the_permalink().'">'.$collection.'</a></div>
                         <h3 class="floorte-color">'.$color_name.'</h3>
                     </div>
                 </div>
             </div>';
 
         endwhile;
         $outpout .= '</div></div>';
         wp_reset_query();
     }  
 
 
     return $outpout;
 }
 add_shortcode( 'featured_products_slider', 'featured_products_slider' );

 //Brand Banner header Shortcode

function brand_header_banner_function($arg)
{ 
    $storename = do_shortcode('[Retailer "companyname"]');

    if(in_array('floorte', $arg)){

        $content = '<img src="https://mmllc-images.s3.us-east-2.amazonaws.com/floorte/floorte-header.jpg" alt="Floorte waterproof hardwood flooring for home | '.$storename.'" title="" itemprop="image"  />';
    }  
       return $content;  
}

add_shortcode('brand_header_banner', 'brand_header_banner_function');

//Floorte Shortcode for alt 
function brand_roomscene_function($arg)
{ 
    $storename = do_shortcode('[Retailer "companyname"]');

    if(in_array('floorte', $arg)){

        $content = '<img src="https://mmllc-images.s3.us-east-2.amazonaws.com/floorte/roomscenes.jpg" alt="Floorte waterproof hardwood flooring for your home | '.$storename.'" title="" itemprop="image"  />';
    }  
       return $content;  
}

add_shortcode('brand_roomscene', 'brand_roomscene_function');

//Floorte Shortcode for alt 
function brand_contact_function($arg)
{ 
    $storename = do_shortcode('[Retailer "companyname"]');

    if(in_array('floorte', $arg)){

        $content = '<img src="https://mmllc-images.s3.us-east-2.amazonaws.com/floorte/Floorte-Hardwood-2-tier-Rendering_FINAL.jpg" alt="Floorte waterproof hardwood flooring display | '.$storename.'" title="" itemprop="image"  />';
    }  
       return $content;  
}

add_shortcode('brand_contact', 'brand_contact_function');

//Floorte Shortcode for alt 
function brand_construction_function($arg)
{ 
    $storename = do_shortcode('[Retailer "companyname"]');

    if(in_array('floorte', $arg)){

        $content = '<img src="https://mmllc-images.s3.us-east-2.amazonaws.com/floorte/Group-6.jpg" alt="Floorte waterproof hardwood flooring construction | '.$storename.'" title="" itemprop="image"  />';
    }
          return $content;  
}

//Floorte Shortcode for alt 
add_shortcode('brand_construction', 'brand_construction_function');

function brand_construction_mobile_function($arg)
{ 
    $storename = do_shortcode('[Retailer "companyname"]');

    if(in_array('floorte', $arg)){

        $content = '<img src="https://mmllc-images.s3.us-east-2.amazonaws.com/floorte/floorte-layers-mobile.jpg" alt="Floorte waterproof hardwood flooring construction | '.$storename.'" title="" itemprop="image"  />';
    }  

       return $content;  
}

add_shortcode('brand_construction_mobile', 'brand_construction_mobile_function');

function brand_resistant_function($arg)
{ 
    $storename = do_shortcode('[Retailer "companyname"]');

    if(in_array('floorte', $arg)){

        $content = '<img src="https://mmllc-images.s3.us-east-2.amazonaws.com/floorte/floorte-scuf-pic.jpg" alt="Floorte waterproof hardwood flooring with stain resistant finish | '.$storename.'" title="" itemprop="image"  />';
    }   
       return $content;  
}

add_shortcode('brand_resistant', 'brand_resistant_function');


//accessiBe script hook with cde parameter
$website_json_data = json_decode(get_option('website_json'));

foreach($website_json_data->sites as $site_cloud){
            
    if($site_cloud->instance == 'prod'){

        if( $site_cloud->accessibe == 'true'){

            add_action('fl_body_close', 'accessiBe_custom_footer_js');

        }

    }
}

function accessiBe_custom_footer_js() {
    
  echo "<script > (function(document, tag) {
    var script = document.createElement(tag);
    var element = document.getElementsByTagName('body')[0];
    script.src = 'https://acsbap.com/api/app/assets/js/acsb.js';
    script.async = true;
    script.defer = true;
    (typeof element === 'undefined' ? document.getElementsByTagName('html')[0] : element).appendChild(script);
    script.onload = function() {
        acsbJS.init({
            statementLink: '',
            feedbackLink: '',
            footerHtml: 'Enable powered by <a href=https://enablemysite.com/ target=_blank>enablemysite</a>',
            hideMobile: false,
            hideTrigger: false,
            language: 'en',
            position: 'left',
            leadColor: '#c8c8c8',
            triggerColor: '#146ff8',
            triggerRadius: '50%',
            triggerPositionX: 'left',
            triggerPositionY: 'bottom',
            triggerIcon: 'wheel_chair4',
            triggerSize: 'medium',
            triggerOffsetX: 20,
            triggerOffsetY: 20,
            mobile: {
                triggerSize: 'small',
                triggerPositionX: 'left',
                triggerPositionY: 'center',
                triggerOffsetX: 0,
                triggerOffsetY: 0,
                triggerRadius: '0'
            }
        });
    };
}(document, 'script')); </script>";
}

add_action('fl_body_close', 'mmsession_custom_footer_js');
function mmsession_custom_footer_js() {
    
  echo "<script src='https://session.mm-api.agency/js/mmsession.js'></script>";
}

//chat meter Shortcode code start here

function chatmeter_function($arg)
{ 
    $website_json =  json_decode(get_option('website_json'));
     

     foreach($website_json->sites as $site_chat){

        if($site_chat->instance == 'prod'){

            return $site_chat->chatmeter;

        }
     } 
}

add_shortcode('chat_meter', 'chatmeter_function');

//chat meter Shortcode code end here

//Wells Fargo Financing link Shortcode code start here

function wells_fargo_function($arg)
{ 
    $website_json =  json_decode(get_option('website_json'));  
    
    $finlink = '';
     
    if($website_json->financeSync || $website_json->financeWF){
        
        if($website_json->financeWF){ $finlink = $website_json->financeWF;  }else{ $finlink = $website_json->financeSync; }        

        $content = '<a href="'.$finlink.'" target="_blank" class="fl-button financeSync" role="button" rel="noopener">
                         <span class="fl-button-text">'.$arg[0].'</span>
                    </a>';
                    
    }else{

        $content = '';

    }   

    return $content;
  
}

add_shortcode('wells_fargo_finance', 'wells_fargo_function');



function wells_fargo_function_link($arg)
{ 
    $website_json =  json_decode(get_option('website_json'));  
    
    $finlink = '';
     
    if($website_json->financeSync || $website_json->financeWF){
        
        if($website_json->financeWF){ 
            $finlink = $website_json->financeWF;  
        }else{ 
            $finlink = $website_json->financeSync; 
        }
                    
    }else{

        $finlink = '';

    }   

    return $finlink;
  
}

add_shortcode('wells_fargo_finance_link', 'wells_fargo_function_link');

//Synchrony Financing link Shortcode code start here

function synchrony_function($arg)
{ 
    $website_json =  json_decode(get_option('website_json'));    

    $finlink = '';

    if($website_json->financeSync || $website_json->financeWF){

        if($website_json->financeSync){ $finlink = $website_json->financeSync;  }else{ $finlink = $website_json->financeWF; }

        $content = '<a href="'.$website_json->financeSync.'" target="_blank" class="fl-button financeSync" role="button" rel="noopener">
                         <span class="fl-button-text">'.$arg[0].'</span>
                    </a>';

                    
    }else{

        $content = '';

    }

    return $content;

}

add_shortcode('synchrony_finance', 'synchrony_function');


function synchrony_function_link($arg)
{ 
    $website_json =  json_decode(get_option('website_json'));    

    $finlink = '';

    if($website_json->financeSync || $website_json->financeWF){

        if($website_json->financeSync){ 
            $finlink = $website_json->financeSync;  
        }else{ 
            $finlink = $website_json->financeWF;
         }       
                    
    }else{

        $finlink = '';

    }

    return $finlink;

}

add_shortcode('synchrony_finance_link', 'synchrony_function_link');

//Area rug affilate Button

function arearug_affiliate_button($arg){  
    $website_json =  json_decode(get_option('website_json'));
    $rugAffiliateCode = isset($website_json->rugAffiliateCode)?$website_json->rugAffiliateCode:"";

    $loc_country = $website_json->locations[0]->country;
    if($loc_country =='US'){ $en = 'en_us'; }elseif($loc_country =='CA'){ $en = 'en_ca'; }

    if($rugAffiliateCode){

        $areabtn = '<div class="rugshopbtn-wrapper">';
        $areabtn .= '<a href="https://rugs.shop/'.$en.'/?store='.$rugAffiliateCode.'" target="_blank" class="fl-button rugshopbtn" role="button">
                            <span class="button-text">'.$arg[0].'</span></a>';
        $areabtn .= '</div>';

    }   
       return $areabtn;  
}

add_shortcode('rugshop_button', 'arearug_affiliate_button');

//Area rug affilate link
function arearug_affiliate_link($arg){  
    
    $website_json =  json_decode(get_option('website_json'));
    $rugAffiliateCode = isset($website_json->rugAffiliateCode)?$website_json->rugAffiliateCode:"";
    $loc_country = $website_json->locations[0]->country;
    if($loc_country =='US'){ $en = 'en_us'; }elseif($loc_country =='CA'){ $en = 'en_ca'; }

    if($rugAffiliateCode){

 $arealnk = '<a href="https://rugs.shop/'.$en.'/?store='.$rugAffiliateCode.'" target="_blank" class="rugshoplink">'.$arg[0].'</a>';

    }
   

       return $arealnk;

  
}

add_shortcode('rugshop_link', 'arearug_affiliate_link');

//Area rug affilate code
function arearug_affiliate_code($arg){  

        $website_json =  json_decode(get_option('website_json'));
        $rugAffiliateCode = isset($website_json->rugAffiliateCode)?$website_json->rugAffiliateCode:"";
    
        if($rugAffiliateCode){
    
            return $rugAffiliateCode;
    
        }
        else{

            return '';
        }   
      
    }    
add_shortcode('affiliate_code', 'arearug_affiliate_code');



//Check image path for swatch and gallery image

function get_image_url ($image,$height,$width){

    if(get_option('cloudinary',true) == 'false' || strpos($image, 'mmllc-images.s3') !== false || strpos($image, 'products.daltile.com') !== false || strpos($image, 's7.shawimg.com') !== false){

        
        if(strpos($image , 's7.shawimg.com') !== false){
            if(strpos($image , 'http') === false){ 
            $image = "http://" . $image;
        }	
           
        }else{
            if(strpos($image , 'http') === false){ 
            $image = "https://" . $image;
        }	
            $image= "https://mm-media-res.cloudinary.com/image/fetch/h_".$height.",w_".$width.",c_limit/".$image."";
        }
    
      }else{
    
        $image= 'https://res.cloudinary.com/mobilemarketing/image/upload/c_limit,h_'.$height.',w_'.$width.',q_auto'.$image;
    
      }	

      return $image;

}

//Single gallery image of product

function single_gallery_image_product($post_id,$height,$width){


    if(get_field('gallery_room_images', $post_id)){			
        
        // loop through the rows of data
        
        $gallery_image = get_field('gallery_room_images', $post_id);

		$gallery_i = explode("|",$gallery_image);
		

			foreach($gallery_i as  $key=>$value) {	
                
                
                $room_image =  get_image_url($value,'1000','1600');
               
                break;
			}
		  
    }

    return $room_image;
}

function thumb_gallery_images_in_plp_loop($value){

    return  get_plp_image_url($value,'222','222');    

}

function get_plp_image_url ($image,$height,$width){

    if(get_option('cloudinary',true) == 'false' || strpos($image, 'mmllc-images.s3') !== false || strpos($image, 'products.daltile.com') !== false || strpos($image, 's7.shawimg.com') !== false){

        
        if(strpos($image , 's7.shawimg.com') !== false){
            if(strpos($image , 'http') === false){ 
            $image = "http://" . $image;
        }	
           
        }else{
            if(strpos($image , 'http') === false){ 
            $image = "https://" . $image;
        }	
            $image= "https://mm-media-res.cloudinary.com/image/fetch/h_".$height.",w_".$width.",c_fill/".$image."";
        }
    
      }else{
    
        $image= 'https://res.cloudinary.com/mobilemarketing/image/upload/c_limit,h_'.$height.',w_'.$width.',q_auto'.$image;
    
      }	

      return $image;

}


//Cloudinary Swatch image high

function swatch_image_product($post_id,$height,$width){

$image = get_post_meta($post_id,'swatch_image_link',true) ? get_post_meta($post_id,'swatch_image_link',true):"http://placehold.it/168x123?text=No+Image"; 
return  get_image_url($image,$height,$width);

}

//Cloudinary Swatch image thumbnail 
function swatch_image_product_thumbnail($post_id,$height){


    $image = get_post_meta($post_id,'swatch_image_link',true) ? get_post_meta($post_id,'swatch_image_link',true):"http://placehold.it/168x123?text=No+Image"; 
    
    return  get_image_url($image,$height,'222');    
    
    }

//New PLP DB Query Cloudinary Swatch image thumbnail 
function newplp_swatch_image_product_thumbnail($image,$height){


    //$image = get_field('swatch_image_link',$post_id) ? get_field('swatch_image_link',$post_id):"http://placehold.it/168x123?text=No+Image"; 
    
    return  get_image_url($image,$height,'222');    
    
    }

//Cloudinary high resolution gallery image 
function high_gallery_images_in_loop($value){

    return  get_image_url($value,'1000','1600');  

}

//Cloudinary thumnail gallery image 
function thumb_gallery_images_in_loop($value){

    return  get_image_url($value,'222','222');    

}


///Seo optimization function according semrush audit report

/**
 * Dequeue the jQuery UI script.
 *
 * Hooked to the wp_print_scripts action, with a late priority (100),
 * so that it is after the script was enqueued.
 */
 function wpdocs_dequeue_script() {
    wp_dequeue_script( 'bb-header-footer' );
}
add_action( 'wp_print_scripts', 'wpdocs_dequeue_script', 100 );

/**
 * Dequeue BB header foooter css and added into grandchild styles.css
 */

function remove_bbhf_css() {   
    wp_dequeue_style( 'bbhf-style-css' );
 }
 add_action( 'wp_print_styles', 'remove_bbhf_css', 100 );

 /**
 * Removed emoji css and js
 */

 function disable_emoji_feature() {
	
	// Prevent Emoji from loading on the front-end
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );

	// Remove from admin area also
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );

	// Remove from RSS feeds also
	remove_filter( 'the_content_feed', 'wp_staticize_emoji');
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji');

	// Remove from Embeds
	remove_filter( 'embed_head', 'print_emoji_detection_script' );

	// Remove from emails
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );

	// Disable from TinyMCE editor. Currently disabled in block editor by default
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );

	/** Finally, prevent character conversion too
         ** without this, emojis still work 
         ** if it is available on the user's device
	 */

	add_filter( 'option_use_smilies', '__return_false' );

}

function disable_emojis_tinymce( $plugins ) {
	if( is_array($plugins) ) {
		$plugins = array_diff( $plugins, array( 'wpemoji' ) );
	}
	return $plugins;
}

add_action('init', 'disable_emoji_feature');

 /**
 * //Remove JQuery migrate
 */
 
function remove_jquery_migrate( $scripts ) {
    if ( ! is_admin() && isset( $scripts->registered['jquery'] ) ) {
         $script = $scripts->registered['jquery'];
    if ( $script->deps ) { 
 // Check whether the script has any dependencies
 
         $script->deps = array_diff( $script->deps, array( 'jquery-migrate' ) );
  }
  }
  }
 add_action( 'wp_default_scripts', 'remove_jquery_migrate' );

 //Custom link shortcode for blog post
function custom_link_shortcode($arg){    
   
    $page = url_to_postid($arg[0]);

    if($page){

        return $arg[0];
    }
    else{
        return '/';
      }       
  
}    
add_shortcode('Link', 'custom_link_shortcode');

//Covid home page banner hook
function bbtheme_covid_banner() {
    $iscovid  =  get_option('covid');
     if ( $iscovid == '1' &&  is_front_page() ) {

          $content = do_shortcode('[fl_builder_insert_layout slug="covid19-banner-row"]');
        
          echo $content;
     }
    
}
add_action( 'fl_content_open', 'bbtheme_covid_banner' );


//Roomvo script
add_action('fl_body_close', 'roomvo_custom_footer_js');
function roomvo_custom_footer_js() {

    $website_json_data = json_decode(get_option('website_json'));

    foreach($website_json_data->sites as $site_cloud){
            
        if($site_cloud->instance == 'prod'){
    
            if( $site_cloud->roomvo == 'true'){
    
  echo "<script src='https://www.roomvo.com/static/scripts/b2b/mobilemarketing.js' async></script>";

            }
        }
    }
}


//Roomvo script integration
function roomvo_script_integration($manufacturer,$sku,$pro_id){

    $website_json_data = json_decode(get_option('website_json'));

    foreach($website_json_data->sites as $site_cloud){
            
        if($site_cloud->instance == 'prod'){
    
            if( $site_cloud->roomvo == 'true'){

                    if($manufacturer == 'Bruce'){ 
                      $manufacturer = get_post_meta( $pro_id, 'brand', true );
                    }
                    elseif($manufacturer == 'AHF'){
                        $manufacturer = get_post_meta( $pro_id, 'brand', true );
                    }
                    elseif($manufacturer  == 'Shaw'){
                        $manufacturer = get_post_meta( $pro_id, 'brand', true );
                    }
                    else{
                        $manufacturer == $manufacturer ;
                    }

                    $manufacturer = str_replace(' ', '', strtolower($manufacturer));  

                    $content='<div id="roomvo">
                            <div class="roomvo-container">
                        <a class="roomvo-stimr button" data-sku="'.$sku.'" style="visibility: hidden;"><i class="fa fa-camera" aria-hidden="true"></i> &nbsp;SEE IN MY ROOM</a>
                            </div>
                    </div>';
       
               $content .='<script type="text/javascript">
                               function getProductSKU() {                       
                               return "'.$manufacturer.'-'.$sku.'";
                               }
                           </script>';
        
                 echo $content;
    
            }
    
        }
    }
   
}



if (!wp_next_scheduled('roomvo_csv_integration_cronjob')) {    
      
    wp_schedule_event( time() +  20800, 'daily', 'roomvo_csv_integration_cronjob');
}
add_action( 'roomvo_csv_integration_cronjob', 'roomvo_csv_integration' );


//Roomvo csv integration
function roomvo_csv_integration(){

    $data = array();
    
    $brandmapping = array(

        "carpeting",
        "hardwood_catalog",
        "laminate_catalog",
        "luxury_vinyl_tile",
        "tile_catalog"
        
    );    

$website_json_data = json_decode(get_option('website_json'));

foreach($website_json_data->sites as $site_cloud){
            
    if($site_cloud->instance == 'prod'){

        if( $site_cloud->roomvo == 'true'){

            $isroomvo = 'true';

        }
    }
}

            if($isroomvo == 'true'){

            $protocols = array('https://', 'https://www.', 'www.','http://', 'http://www.');
            $domain = str_replace($protocols, '', home_url());

            $upload_dir = wp_get_upload_dir();
            $file= $upload_dir['basedir']. '/sfn-data/product_file.csv';

            write_log($file);

            $file = fopen($file, 'w');
            
            // save the column headers
            fputcsv($file, array('Manufacturer', 'Manufacturer SKU Number', 'product page URL', 'Dealer name', 'Dealer website domain'));
            
            foreach($brandmapping as $product_post){

                write_log('<----------'.$product_post.' Started--------->');
                sleep(30);

            // Sample data. This can be fetched from mysql too

            global $wpdb;
            $product_table = $wpdb->prefix."posts";
            $products_data = $wpdb->get_results("SELECT ID FROM $product_table WHERE post_type = '$product_post' AND post_status = 'publish'");


                    $i =1;
                    foreach($products_data as $product) {

                     //   if(get_post_meta( $product->ID, 'manufacturer', true ) != 'Shaw'){ 

                                    if(get_post_meta( $product->ID, 'brand', true ) == 'Bruce'){ 
                                    $manufacturer = get_post_meta( $product->ID, 'brand', true );
                                    }
                                    elseif(get_post_meta( $product->ID, 'manufacturer', true ) == 'AHF'){
                                        $manufacturer = get_post_meta( $product->ID, 'brand', true );
                                    }
                                    elseif(get_post_meta( $product->ID, 'manufacturer', true ) == 'Shaw'){
                                        $manufacturer = get_post_meta( $product->ID, 'brand', true );
                                    }
                                    else{
                                        $manufacturer = get_post_meta( $product->ID, 'manufacturer', true );
                                    }

                                    $sku = get_post_meta( $product->ID, 'sku', true );
                                    $product_url = get_permalink( $product->ID );
                                    $dealer_name = $website_json_data->name;
                                    $domain_url = $domain; 
                                    $manufacturer = str_replace(' ', '', strtolower($manufacturer));

                                    fputcsv($file, array($manufacturer, $sku, $product_url, $dealer_name, $domain_url));

                                    if($i % 5000==0){ write_log($i.'----5000'); ob_flush(); sleep(10);}

                                    $i++;

                       // }
                        
                    }

            wp_reset_query();

            write_log('<----------'.$product_post.' Ended--------->');

            }
            
            $file_size =  $upload_dir['basedir']. '/sfn-data/product_file.csv' ;

            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => "https://www.roomvo.com/pro/api/ext/update_dealer_mappings",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => array(
                'requester' => 'mobilemarketing',
                'api_key' => 'cac7fb3c-7996-499d-8729-96a3547515b8',
                'dealer_name' => $website_json_data->name,
                'dealer_domain' => $domain_url,
                'email' => 'devteam.agency@gmail.com',
                'product_file' => new CURLFILE($file_size)
                ),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: multipart/form-data",
                "Content-Type: application/json"
            ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            write_log('<----------response--------->');
            write_log( $response);
            write_log('<----------response--------->');

            }else{

                write_log('<----------NO ROOMVO CDE SWITCH TRUE--------->');
            }
}

add_filter('the_excerpt', 'do_shortcode');
add_filter('get_the_excerpt', 'shortcode_unautop');
add_filter('get_the_excerpt', 'do_shortcode');

add_filter( 'facetwp_per_page_options', function( $options ) {
    return array( 12,24,36,48,60 );
});

/*Thank you page image*/
function thankyou_responseimage(){

    $promos = json_decode(get_option('promos_json'));

    foreach($promos as $promo){

                foreach($promo->widgets as $widget){                    

                    if($widget->type =='thanks'){

                        $realArray = (array)$widget;                      
                        $realArray1 = (array)$realArray['images'][0];
                        $realArray2 = (array)$realArray['images'][01];

                       // $thanks =  array($promo->promoCode => $realArray1['url']);
                        $thanks =  array( $realArray1['url'], $realArray2['url']);

                    }

                }

    }

    if($_GET['promocode'] !=''){       

        $content = '<img class="fl-photo-img thankuimage-desktop" src="https://'.$thanks[0].'" alt="thank-you-image"  title="thank_you_image">
        <img class="fl-photo-img thankuimage-mobile" src="https://'.$thanks[1].'" alt="thank-you-image"  title="thank_you_image">';

        return $content;
    }
}
//add_shortcode( 'thankyou-responseimage', 'thankyou_responseimage' );


function add_facebook_verify_meta_tags() {
  
    $social_report = json_decode(get_option('social_report'));

        foreach( $social_report as $report){

            if($report->type == 'fbVerify'){              

                echo '<meta name="'.$report->username.'" content="'.$report->accountId.'" />' . "\n";

            }

        }    
    
    }

add_action( 'wp_head', 'add_facebook_verify_meta_tags' , 2 );


//Custom Banner home page banner hook
// function bbtheme_custom_alert_banner() {  

//     $website_json =  json_decode(get_option('website_json'));
     

//      foreach($website_json->sites as $site_chat){

//         if($site_chat->instance == 'prod'){ 

//             if ( $site_chat->banner != null &&  is_front_page() ) {

//                 $content = '<div class="custombanneralert" style="background-color:'.$website_json->color1.';">'.$site_chat->banner.'</div>';
                
//                 echo $content;
//             }
//         }
//     }
    
// }
// add_action( 'fl_content_open', 'bbtheme_custom_alert_banner' );


add_shortcode( 'custom_alert', 'bbtheme_custom_alert_text' );
function bbtheme_custom_alert_text() {  

    $website_json =  json_decode(get_option('website_json'));
     

     foreach($website_json->sites as $site_chat){

        if($site_chat->instance == 'prod'){ 

            if ( $site_chat->banner != null &&  is_front_page() ) {

                $content = '<b class="custom_alert_text">'.$site_chat->banner.'</b>';
                
                echo $content;
            }
        }
    }
    
}
//Custom Banner home page banner hook end here

//Coretec colorwall redirection function
function colorwall_redirect() {

    if( get_option('run_create_colorwall_order_once') !='1' ) {

        global $wpdb;
        
        $table_redirect = $wpdb->prefix.'redirection_items';

        $data_color = array(
        "url" => "^/flooring/luxury-vinyl/coretec-colorwall/choose-your-color/.*",
        "match_url" => "regex",
        "match_data" => '{"source":{"flag_regex":true}}',
        "action_code" => "301",
        "action_type" => "url",
        "action_data" => "/flooring/luxury-vinyl/coretec-colorwall/",
        "match_type" => "url",
        "title" => "Coretec Colorwall luxury color",
        "regex" => "0",
        "group_id" => "1",
        "position" => "0",
        "last_access" => current_time( 'mysql' ),
        "status" => "enabled"
        );

        $data_color_vinyl = array(
            "url" => "^/flooring/vinyl/coretec-colorwall/choose-your-color/.*",
            "match_url" => "regex",
            "match_data" => '{"source":{"flag_regex":true}}',
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => "/flooring/vinyl/coretec-colorwall/",
            "match_type" => "url",
            "title" => "Coretec Colorwall Vinyl color",
            "regex" => "0",
            "group_id" => "1",
            "position" => "0",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled"
            );

        $data_style = array(
            "url" => "^/flooring/luxury-vinyl/coretec-colorwall/choose-your-style/.*",
            "match_url" => "regex",
            "match_data" => '{"source":{"flag_regex":true}}',
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => "/flooring/luxury-vinyl/coretec-colorwall/",
            "match_type" => "url",
            "title" => "Coretec Colorwall luxury style",
            "regex" => "0",
            "group_id" => "1",
            "position" => "0",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled"
            );

            $data_style_vinyl = array(
                "url" => "^/flooring/vinyl/coretec-colorwall/choose-your-style/.*",
                "match_url" => "regex",
                "match_data" => '{"source":{"flag_regex":true}}',
                "action_code" => "301",
                "action_type" => "url",
                "action_data" => "/flooring/vinyl/coretec-colorwall/",
                "match_type" => "url",
                "title" => "Coretec Colorwall vinyl style",
                "regex" => "0",
                "group_id" => "1",
                "position" => "0",
                "last_access" => current_time( 'mysql' ),
                "status" => "enabled"
                );

        $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

        $wpdb->insert($table_redirect,$data_color,$format);        

        $wpdb->insert($table_redirect,$data_style,$format);

        $wpdb->insert($table_redirect,$data_style_vinyl,$format);

        $wpdb->insert($table_redirect,$data_color_vinyl,$format);

        update_option( 'run_create_colorwall_order_once', '1' );

    }
   

}
   
   add_action('wp_head', 'colorwall_redirect');

   //Coretec colorwall redirection function
function beutifall_redirect() {

    if( get_option('run_create_financing_order_once') !='1' ) {

        global $wpdb;

        $check_financing=  get_page_by_path( 'financing', OBJECT, 'page' );     
        
        if($check_financing){
        
                $table_redirect = $wpdb->prefix.'redirection_items';

                $data_color = array(
                "url" => "^/financing/*",
                "match_url" => "regex",
                "match_data" => '{"source":{"flag_regex":true}}',
                "action_code" => "301",
                "action_type" => "url",
                "action_data" => "/flooring-financing/",
                "match_type" => "url",
                "title" => "Beutifall Sale Financing",
                "regex" => "1",
                "group_id" => "1",
                "position" => "0",
                "last_access" => current_time( 'mysql' ),
                "status" => "enabled"
                );

                $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

                $wpdb->insert($table_redirect,$data_color,$format);  

                $check_financing=  get_page_by_path( 'financing', OBJECT, 'page' );     
                
            
                    $financing_post = array(
                        'ID'           => $check_financing->ID,
                        'post_status'   => 'draft',               
                    );

                    // Update the post into the database
                    wp_update_post( $financing_post );
                
                
                update_option('getcouponreplace',0);
                update_option('getcouponreplacetext','');
                update_option('getcouponreplaceurl','');

         }

        update_option( 'run_create_financing_order_once', '1' );

    }   
}
   
add_action('wp_head', 'beutifall_redirect');


function hex2rgba($color, $opacity = false) {

	$default = 'rgb(0,0,0)';

	//Return default if no color provided
	if(empty($color))
          return $default; 

	//Sanitize $color if "#" is provided 
        if ($color[0] == '#' ) {
        	$color = substr( $color, 1 );
        }

        //Check if color has 6 or 3 characters and get values
        if (strlen($color) == 6) {
                $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
        } elseif ( strlen( $color ) == 3 ) {
                $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
        } else {
                return $default;
        }

        //Convert hexadec to rgb
        $rgb =  array_map('hexdec', $hex);

        //Check if opacity is set(rgba or rgb)
        if($opacity){
        	if(abs($opacity) > 1)
        		$opacity = 1.0;
        	$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
        } else {
        	$output = 'rgb('.implode(",",$rgb).')';
        }

        //Return rgb(a) color string
        return $output;
}