<?php
// Variable for Product Loop listing page Column
$col_css = postpercol + 1;

$color = (get_option('api_colorcode_sandbox') != "")?get_option('api_colorcode_sandbox'):"#00247a";
$seleinformation = json_decode(get_option('saleinformation'));
$seleinformation = (array)$seleinformation;

$salespriority = json_decode(get_option('salespriority'));
$salespriority = (array)$salespriority;
$high_priprity = min($salespriority);


if(isset($_GET['promo']) && !empty($_GET['promo'])) {

    $promo_code = $_GET['promo'];

}else{ 

    $promo_code = array_search($high_priprity,$salespriority);
}


$website_json_data = json_decode(get_option('website_json'));
$primary_color = $website_json_data->color1 ;
$secondary_color = $website_json_data->color2 ;
$other_color = $website_json_data->color3 ;
$rgba = hex2rgba($primary_color, 0.60);

?>
<style>

.flooring_products_primary .fl-node-content .uabb-new-ib:before {
    background-color: <?php echo $rgba ; ?> !important;

}
.footer_primary > .fl-row-content-wrap {
    background-color: <?php echo $primary_color; ?>;
}

.footer_primary_copyright .fl-rich-text *{

    color: <?php echo $primary_color; ?> !important;
}
footer div.footer_primary_copyright a:hover{

    color: <?php echo $secondary_color; ?> !important;
}

.footer_primary_copyright .fl-menu *{

color: <?php echo $primary_color; ?> !important;
}

.fl-page-bar {
    
    border-color: <?php echo $primary_color; ?> !important;
    background-color: <?php echo $primary_color; ?> !important;
   
}
.salebanner-slide{
	max-height:440px!important;
	margin: 0 auto;
    display: block;
}
.homepage-banner-bg{
	background-image: url(<?php echo $seleinformation[$promo_code]->background_image;?>)!important;
	background-repeat: no-repeat;
}
.fl-row-content-wrap.nosalebg {    
	background-image: url(<?php echo "https://mmllc-images.s3.us-east-2.amazonaws.com/beautifall-financing/no-promo-background.jpg";?>)!important;
	background-repeat: no-repeat;
    background-position: center center;
    background-attachment: scroll;
    background-size: cover;
}
.fl-row-content-wrap.custombg {    
	background-image: url(<?php echo $seleinformation[$promo_code]->background_image_landing;?>)!important;
	background-repeat: no-repeat;
    background-position: center center;
    background-attachment: scroll;
    background-size: cover;
}
.fl-row-content-wrap.homepage {
    background-image: url(<?php echo $seleinformation[$promo_code]->background_image;?>)!important;
//	background-image: url(<?php echo $seleinformation[$promo_code]->background_image_landing;?>)!important;
	background-repeat: no-repeat;
    background-position: center center;
    background-attachment: scroll;
    background-size: cover;
}

img.salebanner.fl-slide-photo-img {
    max-width: 500px;
}

.banner-mobile{
	display: none;
	text-align:center;
}

@media(max-width:1080px){
.fl-page-header-primary .fl-logo-img{
max-width: 80%;
}
}

@media(max-width:992px){
.fl-page-header-primary .fl-logo-img{
max-width: 50%;
}
.banner-below-heading-n-text h2 span{
font-size: 25px;
line-height: 28px;
}
.banner-deskop{
display: none;
}
.banner-mobile{
	display: block;
}
}
</style>
